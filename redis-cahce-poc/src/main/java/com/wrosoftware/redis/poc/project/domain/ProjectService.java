package com.wrosoftware.redis.poc.project.domain;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.wrosoftware.redis.poc.project.model.dto.ProjectDTO;

@Service
@Transactional
public class ProjectService {

	private ProjectRepository projectRepository;
	private ProjectMapper mapper;
	
	public ProjectService(ProjectRepository projectRepository) {
		this.projectRepository = projectRepository;
		this.mapper = new ProjectMapper();
	}
	
	public ProjectDTO save(ProjectDTO dto) {
		return Optional.ofNullable(dto)
				.map(mapper::mapToEntity)
				.map(projectRepository::save)
				.map(mapper::mapToDTO)
				.orElse(null);
	}

	public ProjectDTO update(Long id, ProjectDTO project) {
		return projectRepository.findById(id)
				.map(entity -> mapper.updateEntity(entity, project))
				.map(mapper::mapToDTO)
				.orElse(null);
	}

	public List<ProjectDTO> getAllProjects() {
		return projectRepository.findAll()
				.stream()
				.map(mapper::mapToDTO)
				.collect(toList());
	}

	public ProjectDTO getProjectById(Long id) {
		return projectRepository.findById(id)
				.map(mapper::mapToDTO)
				.orElse(null);
	}
	
	
}
