package com.wrosoftware.redis.poc.configuration.spring;

public class OperatorContext {
	private static ThreadLocal<Operator> currentTenant = new ThreadLocal<Operator>();

    public static void setCurrentTenant(Operator tenant) {
        currentTenant.set(tenant);
    }

    public static Operator getCurrentTenant() {
        return currentTenant.get();
    }

    public static void clear() {
        currentTenant.remove();
    }
}
