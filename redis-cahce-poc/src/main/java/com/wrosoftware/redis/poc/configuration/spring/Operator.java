package com.wrosoftware.redis.poc.configuration.spring;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Operator {

	private String operatorId;
	private String tenantId;
	
}
