package com.wrosoftware.redis.poc.project.domain;

import com.wrosoftware.redis.poc.project.model.dto.ProjectDTO;
import com.wrosoftware.redis.poc.project.model.entity.Project;

public class ProjectMapper {

	public Project mapToEntity(ProjectDTO dto) {
		return Project.builder()
				.id(dto.getId())
				.created(dto.getCreated())
				.name(dto.getName())
				.build();
	}
	
	public ProjectDTO mapToDTO(Project project) {
		return ProjectDTO.builder()
				.id(project.getId())
				.created(project.getCreated())
				.name(project.getName())
				.build();
	}

	public Project updateEntity(Project entity, ProjectDTO project) {
		entity.setName(project.getName());
		return entity;
	}
	
	
}
