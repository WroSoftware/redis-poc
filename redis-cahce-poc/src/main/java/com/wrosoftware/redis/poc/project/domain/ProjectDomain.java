package com.wrosoftware.redis.poc.project.domain;

import java.util.List;

import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import com.wrosoftware.redis.poc.project.model.dto.ProjectDTO;

@Component
public class ProjectDomain {
	
	private ProjectService projectService;
	
	public ProjectDomain(ProjectService projectService) {
		this.projectService = projectService;
	}

	public ProjectDTO save(ProjectDTO project) {
		return projectService.save(project);
	}

	@CachePut(value="project_cache", key="#id")
	public ProjectDTO update(Long id, ProjectDTO project) {
		return projectService.update(id, project);
	}

	public List<ProjectDTO> getAllProjects() {
		return projectService.getAllProjects();
	}

	@Cacheable(value="project_cache", key="#id")
	public ProjectDTO getProjectById(Long id) {
		System.out.println(String.format("looking for project by id: %s in database", String.valueOf(id)));
		return projectService.getProjectById(id);
	}
}
