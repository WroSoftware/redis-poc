package com.wrosoftware.redis.poc.project.model.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;

@Value
@Builder
@EqualsAndHashCode
public class ProjectDTO implements Serializable{

	private Long id;
	private Date created;
	private String name;
	
	
}
