package com.wrosoftware.redis.poc.project.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wrosoftware.redis.poc.project.model.entity.Project;

@Repository
interface ProjectRepository extends JpaRepository<Project, Long>{

}
