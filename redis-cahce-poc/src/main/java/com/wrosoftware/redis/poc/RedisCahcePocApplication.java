package com.wrosoftware.redis.poc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RedisCahcePocApplication {

	public static void main(String[] args) {
		SpringApplication.run(RedisCahcePocApplication.class, args);
	}

}
