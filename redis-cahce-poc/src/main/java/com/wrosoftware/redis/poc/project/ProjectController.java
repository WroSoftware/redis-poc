package com.wrosoftware.redis.poc.project;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wrosoftware.redis.poc.project.domain.ProjectDomain;
import com.wrosoftware.redis.poc.project.model.dto.ProjectDTO;

@RestController
@RequestMapping("/project")
public class ProjectController {
	
	private ProjectDomain projectDomain;
	
	public ProjectController(ProjectDomain projectDomain) {
		this.projectDomain = projectDomain;
	}

	@PostMapping
	public ProjectDTO saveProject(@RequestBody ProjectDTO project) {
		return projectDomain.save(project);
	}

	@GetMapping("/all")
	public List<ProjectDTO> getAll(){
		return projectDomain.getAllProjects();
	}
	
	@PutMapping("/{id}")
	public void updateProject(@PathVariable("id")Long id, @RequestBody ProjectDTO project) {
		projectDomain.update(id, project);
	}
	
	@GetMapping("/{id}")
	public ProjectDTO getProject(@PathVariable("id") Long id) {
		return projectDomain.getProjectById(id);
	}
}
